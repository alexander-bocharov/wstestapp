# WsClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.0.

## Installation

Install node with npm
````
apt-get install npm
````
or
```
brew install npm
```
than install project deps.

```
npm install
```
## Build apps and packages
```
npm cache clean --force &&
npm run build
```

Depends on your target OS run
```
npm run electron-build:mac - for MacOS
npm run electron-build:deb - for Deb package
```

See your MacOs build in the root folder

Deb package in dist/installers folder
