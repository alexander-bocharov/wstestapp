var WebsockedAdapter = {};
WebsockedAdapter.socket = null;
WebsockedAdapter.constructor = function (url) {
  this.socket = new WebSocket(url);
  var that = this;
  this.socket.addEventListener('open', function () {
    self.postMessage({command: 'open'});
    setInterval(function () {
      self.postMessage({command: 'state', payload: that.socket.readyState})
    }, 1000);
  });
  this.socket.addEventListener('message', function (message) {
    self.postMessage({command: 'message', payload: message.data});
  });

  this.socket.addEventListener('close', function () {
    self.postMessage({command: 'close'});
  });
  this.socket.addEventListener('error', function (error) {
    self.postMessage({command: 'error', message: error});
  });
  return socket;
};

var socket;
self.onmessage = function (message) {
  var postMessage = message.data;
  switch (postMessage.command) {
    case 0:
      socket = new WebsockedAdapter.constructor(postMessage.payload.url);
      break;
    case 1:
      console.log(postMessage);
      socket.socket.send(postMessage.payload);
      break;
    case 2:
      socket.socket.close();
      break;
  }

};
