import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {WsClient} from '../../services/ws/wsClient';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
    @Input() client: WsClient;
    @Input() clientId: any;
    @Output() close: EventEmitter<null> = new EventEmitter<null>();
    @ViewChild('messages', {static: false}) messagesEl;
    message: string = '';
    messageCode: number;
    events: string = '';

    constructor() {
    }

    ngOnInit() {
        this.client.initConnection();
        this.client.messages$.subscribe((message) => {

            this.events += `[${message.code}]: ${message.data}\n\n`;
            setTimeout(() => {
                this.messagesEl.nativeElement.scrollTop = this.messagesEl.nativeElement.scrollHeight
            }, 100);

        });
    }

    sendMessage() {
        if (this.message.length) {
            this.client.send({command: this.messageCode, data: this.message});
            this.message = '';
        }
    }

    destroy() {
        this.close.emit();
    }

    disableSend(): boolean {
        return !this.message.length || isNaN(this.messageCode)
    }


}
