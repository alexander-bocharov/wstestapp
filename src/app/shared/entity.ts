export enum WsEvent {
  OPEN = 'open',
  ERROR = 'error',
  CLOSE = 'close',
  MESSAGE = 'message',
  STATE = 'state',
}

export interface WsMessage {
  code: number;
  data: any;
}

export interface WsCommand {
  command: number;
  data: any;
}


export enum WsWorkerCommands {
  CONNECT,
  SEND,
  CLOSE,
}
