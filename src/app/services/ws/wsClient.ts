import {EventEmitter} from '@angular/core';
import {WsCommand, WsEvent, WsMessage, WsWorkerCommands} from '../../shared/entity';
import {Subject} from 'rxjs';


export class WsClient {
    connectionState: number;
    wsWorker: Worker;
    private _wsEvent: { [name: string]: EventEmitter<WsMessage | any> } = {};
    messages$: Subject<WsMessage> = new Subject();

    constructor(public wsUrl, public clientId: any) {
    }

    createEmitters() {
        this._wsEvent = {
            'open': new EventEmitter<any>(),
            'close': new EventEmitter<any>(),
            'error': new EventEmitter<any>(),
            'message': new EventEmitter<WsMessage>(),
            'state': new EventEmitter<any>(),
        };
    }

    public reConnect() {
        this._wsEvent[WsEvent.OPEN].unsubscribe();
        this._wsEvent[WsEvent.CLOSE].unsubscribe();
        this._wsEvent[WsEvent.ERROR].unsubscribe();
        this._wsEvent[WsEvent.MESSAGE].unsubscribe();
        this.initConnection();
    }

    public initConnection(): void {
        this.createEmitters();
        this.create();
        this.hookEvents();
    }

    private create() {

        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'assets/js/wssworker.js');
        xhr.onload = () => {
            if (xhr.status === 200) {
                const workerSrcBlob = new Blob([xhr.responseText], {type: 'text/javascript'});
                const workerBlobURL = window.URL.createObjectURL(workerSrcBlob);

                this.wsWorker = new Worker(workerBlobURL);
                this.wsWorker.addEventListener('message', (event: MessageEvent) => {
                    this._wsEvent[event.data.command].emit(event.data.payload);
                });
                this.wsWorker.postMessage({command: WsWorkerCommands.CONNECT, payload: {url: this.wsUrl}});

            }
        };
        xhr.send();
    }

    hookEvents() {
        this._wsEvent[WsEvent.STATE].subscribe((state) => {
            this.connectionState = state;
        });
        this._wsEvent[WsEvent.MESSAGE].subscribe((message) => {
            this.handleMessage(message);
        });

        this._wsEvent[WsEvent.ERROR].subscribe((error) => {
            this.wsWorker.postMessage({command: WsWorkerCommands.CLOSE});
        });

        this._wsEvent[WsEvent.CLOSE].subscribe((error) => {
            this.handleClose(error);
        });

        this._wsEvent[WsEvent.OPEN].subscribe((event) => {
        });
    }


    handleMessage(message: WsEvent) {
        debugger;
        try {
            this.messages$.next(JSON.parse(message));
        } catch (e) {
            this.messages$.next({code: -1, data: message});
        }
    }

    handleClose(error: any) {
        this.wsWorker.terminate();
        this.reConnect();
    }

    getConnectionState(): string {

        switch (this.connectionState) {
            case WebSocket.OPEN:
                return 'open';
            case WebSocket.CONNECTING:
                return 'connecting';
            case WebSocket.CLOSED:
                return 'closed';
            case WebSocket.CLOSING:
                return 'closing';

        }

        return 'closed';
    }

    send(message: WsCommand) {
        try {
            this.wsWorker.postMessage({command: WsWorkerCommands.SEND, payload: JSON.stringify(message)});
        } catch (e) {
            console.log(e)
        }
    }

    disconnect() {
        this.wsWorker.postMessage({command: WsWorkerCommands.CLOSE});
        this.wsWorker.terminate();
    }


}
