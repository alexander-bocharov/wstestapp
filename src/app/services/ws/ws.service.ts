import {Injectable} from '@angular/core';
import {WsClient} from './wsClient';

@Injectable()
export class WsService {


    clients: WsClient[] = [];

    constructor() {
    }

    createClient(url: string) {
        this.clients.push(new WsClient(url, this.clients.length));
    }

    removeClient(id: any) {
        const clientIndex = this.clients.findIndex((client) => {
            return client.clientId === id
        });
        if (clientIndex > -1) {
            this.clients[clientIndex].disconnect();
            this.clients.splice(clientIndex, 1);
        }
    }


}
