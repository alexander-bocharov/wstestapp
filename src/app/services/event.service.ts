import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class EventService {



  private _wsConnected: Subject<boolean> = new Subject<boolean>();
  public onWsConnected: Observable<boolean> = this._wsConnected.asObservable();


  constructor() {
  }

  wsConnectedEvent() {
    this._wsConnected.next(true);
  }



}
