import {Component, OnInit} from '@angular/core';
import {WsService} from './services/ws/ws.service';
import {WsClient} from './services/ws/wsClient';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'WsClient';
    url: string = 'ws://funnels-viewer.ads.svc.k8s.devel/api/v1/ws';

    constructor(public ws: WsService) {

    }

    ngOnInit(): void {

    }

    createClient() {
        this.ws.createClient(this.url);
    }

    removeClient(id: any) {
        this.ws.removeClient(id);
    }
}
